#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

//' @keywords internal
// [[Rcpp::export]]
arma::mat lambda_matrix_loop( arma::mat dq, arma::mat de, arma::cube Hq, arma::cube He, arma::colvec xbq,
                              arma::vec G1_prime_xq, arma::vec G2_xe, arma::vec G2_prime_xe,
                              arma::vec G2_prime_prime_xe, arma::vec density, double alpha, arma::vec cdf, bool misspec ) {
  int n = dq.n_rows;
  int k = dq.n_cols;

  // Define some 0-matrices
  arma::mat lambda = arma::zeros<arma::mat>(k, k);
  arma::mat dqi, dei, Hqi, Hei, xxq, xxe, xxqe, xxeq;
  double xbqi, cdfi;

  // Compute the matrix elements
  for (int i = 0; i < n; i++) {

    // Link
    xbqi = xbq(i);

    // Nabla
    dqi = dq.row(i);
    dei = de.row(i);
    xxq = dqi.t() * dqi;
    xxe = dei.t() * dei;
    xxqe = dqi.t() * dei;
    xxeq = dei.t() * dqi;

    // Hessian
    Hqi = Hq.slice(i);
    Hei = He.slice(i);

    // cdf
    cdfi = cdf(i);

    if (misspec){

      lambda += xxq * (G1_prime_xq(i) + G2_xe(i) / alpha) * density(i) +
                xxe * G2_prime_xe(i) +
                Hqi * (G1_prime_xq(i) + G2_xe(i) / alpha)*(cdfi - alpha) +
                (xxqe + xxeq) * G2_prime_xe / alpha * (cdfi - alpha) +
                xxe * (Hei*G2_prime_xe(i) + G2_prime_prime_xe(i)* xbqi *(cdfi - alpha)/alpha);


    } else {

      lambda += xxq * (G1_prime_xq(i) + G2_xe(i) / alpha) * density(i) +
                xxe * G2_prime_xe(i);

    }

  }

  lambda = lambda / n;

  return lambda;
}


//' @keywords internal
// [[Rcpp::export]]
arma::mat sigma_matrix_loop(
    arma::mat dq, arma::mat de, arma::colvec xbq, arma::colvec xbe,
    arma::colvec G1_prime_xq, arma::colvec G2_xe, arma::colvec G2_prime_xe,
    arma::colvec conditional_variance, arma::vec cdf, double alpha, bool misspec) {
  int n = dq.n_rows;
  int k = dq.n_cols;


  // Define some 0-matrices
  arma::mat sigma = arma::zeros<arma::mat>(k, k);
  arma::mat dqi, dei, xxq, xxe, xxeq, xxqe, _sigma_11, _sigma_21, _sigma_22;
  double xbqi, xbei, cdfi;

  // Compute the matrix elements
  for (int i = 0; i < n; i++) {
    dqi = dq.row(i);
    dei = de.row(i);
    xxq = dqi.t() * dqi;
    xxe = dei.t() * dei;
    xxeq = dei.t() * dqi;
    xxqe = dqi.t() * dei;
    xbqi = xbq(i);
    xbei = xbe(i);

    cdfi = cdf(i);

    _sigma_11 = xxq * pow(alpha*G1_prime_xq(i) + G2_xe(i), 2);
    _sigma_21 = (xxeq + xxqe) * (alpha*G1_prime_xq(i) + G2_xe(i)) * G2_prime_xe(i);
    _sigma_22 = xxe * pow(G2_prime_xe(i), 2);

    if (misspec){

      sigma += _sigma_11 * ((1-alpha)/alpha + (1 - 2 * alpha)*(cdfi - alpha)/alpha) +
               _sigma_21 * ((1-alpha)/alpha * (xbqi - xbei) + (1-alpha)/alpha*xbqi*(cdfi-alpha)/alpha - (cdfi-alpha)/alpha *(xbqi-xbei) ) +
               _sigma_22 * (conditional_variance(i)/alpha + (1-alpha)/alpha * pow(xbqi - xbei, 2) - 2*(xbqi-xbei)*xbqi*(cdfi-alpha)/alpha );

    } else {

    sigma += _sigma_11 * (1-alpha)/alpha +
             _sigma_21 * (1-alpha)/alpha * (xbqi - xbei) +
             _sigma_22 * (conditional_variance(i)/alpha + (1-alpha)/alpha * pow(xbqi - xbei, 2));

    }

  }

  // Fill the matrices
  sigma  = sigma / n;

  return sigma;
}



//' @keywords internal
// [[Rcpp::export]]
arma::mat estimating_function_loop(
    arma::colvec y, arma::colvec xbq, arma::colvec xbe,
    arma::mat dq, arma::mat de, arma::colvec G1_prime_xq, arma::colvec G2_xe,
    arma::colvec G2_prime_xe, double alpha) {
  int n = dq.n_rows;
  int k = dq.n_cols;

  // Initialize variables
  arma::mat psi = arma::zeros<arma::mat>(n, k);
  arma::rowvec dqi, dei;
  double yi, xbqi, xbei, hit;


  // Compute the matrix elements
  for (int i = 0; i < n; i++) {
    yi = y(i);
    // Nabla
    dqi = dq.row(i);
    dei = de.row(i);
    // link function
    xbqi = xbq(i);
    xbei = xbe(i);

    // Hit variable
    hit = yi <= xbqi;

    // Fill the matrix
    psi.submat(i, 0, i, k-1)  = dqi * (G1_prime_xq(i) + G2_xe(i)/alpha) * (hit - alpha) + dei *  G2_prime_xe(i) * (xbei - xbqi + (xbqi - yi) * hit / alpha);
  }

  return psi;
}


//' @keywords internal
// [[Rcpp::export]]
arma::mat sigma_matrix_OGP_loop(
    arma::colvec y, arma::colvec xbq, arma::colvec xbe,
    arma::mat dq, arma::mat de, arma::colvec G1_prime_xq, arma::colvec G2_xe,
    arma::colvec G2_prime_xe, double alpha) {
  int n = dq.n_rows;
  int k = dq.n_cols;

  // Initialize variables
  arma::mat sigma = arma::zeros<arma::mat>(k, k);
  arma::rowvec dqi, dei, psi;
  double yi, xbqi, xbei, hit;


  // Compute the matrix elements
  for (int i = 0; i < n; i++) {
    yi = y(i);
    // Nabla
    dqi = dq.row(i);
    dei = de.row(i);
    // link function
    xbqi = xbq(i);
    xbei = xbe(i);

    // Hit variable
    hit = yi <= xbqi;

    // Fill the matrix
    psi =   dqi * (G1_prime_xq(i) + G2_xe(i)/alpha) * (hit - alpha)
          + dei *  G2_prime_xe(i) * (xbei - xbqi + (xbqi - yi) * hit / alpha);

    sigma += psi.t() * psi;
  }

  sigma = sigma / n;

  return sigma;
}


#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

//' @keywords internal
// [[Rcpp::export]]
arma::mat lambda_quantile_loop( arma::mat dq, arma::mat de,
                                arma::vec density_alpha, arma::vec density_beta ) {
  int n = dq.n_rows;
  int k = dq.n_cols;

  // Define some 0-matrices
  arma::mat lambda = arma::zeros<arma::mat>(k, k);
  arma::mat dqi, dei, xxq, xxe;

  // Compute the matrix elements
  for (int i = 0; i < n; i++) {
    // Nabla
    dqi  = dq.row(i);
    dei  = de.row(i);
    xxq  = dqi.t() * dqi;
    xxe  = dei.t() * dei;


    lambda += xxq * density_alpha(i) + xxe * density_beta(i);

  }

  lambda = lambda / n;

  return lambda;
}


//' @keywords internal
// [[Rcpp::export]]
arma::mat sigma_quantile_loop(
    arma::mat dq, arma::mat de, double alpha, double beta) {
  int n = dq.n_rows;
  int k = dq.n_cols;

  // Define some 0-matrices
  arma::mat sigma = arma::zeros<arma::mat>(k, k);
  arma::mat dqi, dei, xxq, xxe, xxeq, xxqe;


  // Compute the matrix elements
  for (int i = 0; i < n; i++) {
    dqi  = dq.row(i);
    dei  = de.row(i);
    xxq  = dqi.t() * dqi;
    xxe  = dei.t() * dei;
    xxeq = dei.t() * dqi;
    xxqe = dqi.t() * dei;

    sigma += alpha * (1-alpha) * xxq + beta * (1-beta) * xxe + beta * (1-alpha) * (xxqe + xxeq);

  }


  sigma = sigma / n;

  return sigma;
}
