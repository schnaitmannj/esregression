# esregression

Simultaneous modeling of the quantile and the expected shortfall of a response variable given a set of covariates for various link functions (e.g. linear, affine, convex, nonlinear and nocrossing), Dimitriadis and Schnaitmann (2021) and Dimitriadis, Liu and Schnaitmann (2021). This package extends the [esreg ](https://github.com/BayerSe/esreg) package created by Sebastian Bayer for Dimitriadis and Bayer (2019) by general link functions. This package allows for constrained estimation of the combination weights (e.g. for the convex and nocrossing link function).


Installation
------------

### GitLab (development)

The latest version of the package is under development at
[GitLab](https://gitlab.com/schnaitmannj/esregression). You can install the
development version using these commands:

    install.packages("devtools")
    devtools::install_gitlab("schnaitmannj/esregression")

If you are using Windows, you need to install the
[Rtools](https://cran.r-project.org/bin/windows/Rtools/) for compilation
of the codes.

Examples
--------

    # Simulate returns from a GARCH(1,1) model with normal innovations.
    # Get VaR and ES forecasts from a GARCH(1,1) (true model) and a GJR-GARCH(1,1)

    alpha <- 0.025

    GARCH_spec_1 <- rugarch::ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)),
    fixed.pars=list(omega=0.04, alpha1=0.1, beta1=0.85) )
    GARCH_spec_2 <- rugarch::ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)),
    fixed.pars=list(omega=0.04, alpha1=0.05, beta1=0.8, gamma1 = 0.1), variance.model = list(model = "gjrGARCH"))

    GARCH_sim_1  <- rugarch::ugarchpath(GARCH_spec_1 , n.sim=2000, m.sim=1, n.start=1000, startMethod="unconditional")
    VaR_FC_1     <- qnorm(alpha) * rugarch::sigma(GARCH_sim_1)
    ES_FC_1      <- - dnorm(qnorm(alpha))/alpha * rugarch::sigma(GARCH_sim_1)

    GARCH_sim_2  <- rugarch::ugarchpath(GARCH_spec_2 , n.sim=2000, m.sim=1, n.start=1000, startMethod="unconditional")
    VaR_FC_2     <- qnorm(alpha) * rugarch::sigma(GARCH_sim_2)
    ES_FC_2      <- -dnorm(qnorm(alpha))/alpha * rugarch::sigma(GARCH_sim_2)

    # simulated returns
    r <- rnorm(2000) * as.numeric(rugarch::sigma(GARCH_sim_1))

    # VaR and ES forecasts
    Xq <- cbind(VaR_FC_1,VaR_FC_2)
    Xe <- cbind(ES_FC_1,ES_FC_2)

    # True quantile and expected shortfall regression parameters (for alpha=0.025)
    true_linear <- c(0,1,0,0,1,0)
    true_convex <- c(1,1,0,0)

    # Estimate the model using the standard settings
    fit_linear <- esregression(r ~ Xq | Xe, alpha = alpha, link = "linear")
    cov <- vcov(fit_linear)

    print("esregression estimator with linear link function")
    print(cbind(Truth=true_linear, Estimate=coef(fit_linear)))

    # Estimate the model using convex combination
    fit_convex <- esregression(r ~ Xq | Xe, alpha = alpha, link = "convex")
    cov <- vcov(fit_convex)

    print("esregression estimator with convex link function")
    print(cbind(Truth=true_convex, Estimate=coef(fit_convex)))


References
----------

[A Joint Quantile and Expected Shortfall Regression
Framework](https://projecteuclid.org/euclid.ejs/1559872834)

[Forecast Encompassing Test for the Expected Shortfall](https://doi.org/10.1016/j.ijforecast.2020.07.008)

[Encompassing Tests for Value at Risk and Expected Shortfall Multi-Step Forecasts based on Inference on the Boundary](https://arxiv.org/abs/2009.07341)
